package programas;

import java.util.Scanner;

import clases.GestorClases;

public class Principal {
	
	/*
	 * @author Iv�n Calvo Burguete
	 */
	
	/*
	 * Atributos del programa princial
	 * @param escaner	es el lector para poder introducir datos a mano
	 * @param gestor	es el objeto que usaremos para poder llamar a los m�todos creados en la clase "GestorClases"
	 */
	static Scanner escaner = new Scanner(System.in);
	static GestorClases gestor = new GestorClases();
	/*
	 * Estructura del programa en general main
	 */
	public static void main(String[] args) {
		altaPersonajesArmas();
		menuPrincipal();
	}
	
	/*
	 * Metodo que muestra las opciones del men� principal del programa
	 */
	static void menuPrincipal() {
		int opcion;
		do {
			System.out.println("---------------------------");
			System.out.println("Programa de Iv�n Calvo");
			System.out.println("1- Gestion Personajes");
			System.out.println("2- Gestion Armas");
			System.out.println("3- Salir");
			System.out.println("---------------------------");
			opcion = escaner.nextInt();
			switch(opcion) {
			case 1:
				opcionesPersonaje();
				break;
			case 2:
				opcionesArma();
				break;
			case 3:
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Opci�n no v�lida");
			}

		} while (opcion != 3);
	}
	/*
	 * Metodo que muestra las opciones del men� de personajes
	 */
	static void opcionesPersonaje() {
		int option;
		do {
			System.out.println("------------------------------------------------");
			System.out.println("1- Crear Personaje fichero");
			System.out.println("2- Visualizar Fichero Personaje");
			System.out.println("3- Buscar Personaje en fichero");
			System.out.println("4- Encontrar al personaje con m�s da�o");
			System.out.println("5- Modificar nombre de personaje en may�sculas");
			System.out.println("6- Contar cu�ntos personajes han sido creados");
			System.out.println("7- Salir");
			option = escaner.nextInt();
			menuPersonaje(option);
		} while (option != 7);
	}
	/*
	 * Metodo con un men� y llama a varios metodos de la clase "GestorClases"
	 * Este m�todo trabaja con ficheros normales, este metodo trabaja con metodos relacionados con la clase Personaje
	 */
	static void menuPersonaje(int option) {
		switch(option) {
		case 1:
			gestor.listarPersonajes();
			break;
		case 2:
			gestor.guardarDatos();
			gestor.cargarDatosPersonaje();
			gestor.verFichero();			
			break;
		case 3:
			gestor.buscarPersonaje();
			break;
		case 4:
			gestor.personajeConMasDmg();
			break;
		case 5:
			gestor.cambiarNombreMayus();
			break;
		case 6:
			gestor.contarCantidadPersonajes();
			break;
		case 7:
			System.out.println("Saliendo del menu de Personaje....");
			break;
		default:
			System.out.println("Opci�n incorrecta");
			break;
		}
	}
	/*
	 * Metodo que contiene las opciones del menu de armas
	 */
	static void opcionesArma() {
		int option;
		do {
		System.out.println("------------------------------------------------");
		System.out.println("1- Crear arma");
		System.out.println("2- Visualizar fichero Arma");
		System.out.println("3- Mostrar nombres fichero Armas");
		System.out.println("4- Mostrar la longitud del fichero Arams");
		System.out.println("5- Salir");
		option = escaner.nextInt();
		menuArma(option);
		} while(option != 5);
	}
	/*
	 * Metodo que tiene un men� con metodos llamados de la clase "GestorClases"
	 * Este metodo trabaja con ficheros aleatorios y metodos relacionados con la clase Arma
	 */
	static void menuArma(int option) {
		switch(option){
		case 1:
			gestor.altaArmaFichero();
			break;
		case 2:
			gestor.verFicheroArmas();
			break;
		case 3:
			gestor.mostrarNombreArmas();
			break;
		case 4:
			gestor.longitudDelFichero();
			break;
		case 5:
			
			break;
		default:
			System.out.println("Opci�n no v�lida");
			break;
		}
	}
	/*
	 * Metodo para dar valores de Personajes y Armas predise�ados
	 */
	static void altaPersonajesArmas() {
		gestor.altaPersonaje("Guerrero", "Halfonso", 70, 100);
		gestor.altaPersonaje("Escudero", "Nekro", 40, 120);
		gestor.altaPersonaje("Arquero", "NoobMaster", 30, 80);
		gestor.altaPersonaje("Sanador", "Pepe", 30, 150);
		gestor.altaPersonaje("Mago", "Damipro", 120, 90);
		gestor.altaArma("gladiator", 12, 80);
		gestor.altaArma("alabarda", 56, 90);
		gestor.altaArma("bambina", 35, 12);
		gestor.altaArma("surcadora", 75, 5);
		gestor.altaArma("winter", 34, 67);
	}
}
