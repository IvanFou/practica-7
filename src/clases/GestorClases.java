package clases;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class GestorClases {
	
	static Scanner escaner = new Scanner(System.in);
	/*
	 * @author Iv�n Calvo Burguete
	 */
	
	/*
	 * ArrayLists que almacenar�n los datos de Personaje y Arma
	 */
	private ArrayList<Personaje> listaPersonajes;
	private ArrayList<Arma> listaArmas;
	
	/*
	 * Constructor de la clase Gestor
	 * con los 2 arrayList 
	 */
	public GestorClases() {
		this.listaPersonajes = new ArrayList<Personaje>();
		this.listaArmas = new ArrayList<Arma>();
	}

	/*
	 * Metodo para ver el ArrayList de la clase Personaje
	 * return listaPersonajes
	 */
	public ArrayList<Personaje> getListaPersonajes() {
		return listaPersonajes;
	}
	/*
	 * Metodo para dar valor al ArayList de la clase Personaje
	 */
	public void setListaPersonajes(ArrayList<Personaje> listaPersonajes) {
		this.listaPersonajes = listaPersonajes;
	}
	/*
	 * Metodo para ver el ArrayList de la clase Arma
	 * return listaArmas
	 */
	public ArrayList<Arma> getListaArmas() {
		return listaArmas;
	}
	/*
	 * Metodo para dar valor al ArrayList de la clase Arma
	 */
	public void setListaArmas(ArrayList<Arma> listaArmas) {
		this.listaArmas = listaArmas;
	}
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	// PERSONAJES
	
	/*
	 * Metodo para crear/dar de alta un personaje introduciendo todos sus datos
	 * recibe:
	 * @param codigo	codigo de personaje
	 * @param nombre	nombre de personaje
	 * @param dmg		da�o de personaje
	 * @param hp		vida de personaje
	 */
	public void altaPersonaje(String codigo, String nombre, int dmg, int hp) {
			Personaje personaje = new Personaje(codigo, nombre, dmg, hp);
				listaPersonajes.add(personaje);
		}
	
	/*
	 * Metodo para listar los Personajes del ArrayList listaPersonajes
	 */
	public void listarPersonajes() {
		for (Personaje personaje : listaPersonajes) {
			if (personaje != null) {
				System.out.println(personaje);
			} else {
				System.out.println("Vaya, est� vacia la lista :(");
			}
		}
	}
	/*------------------------------------------------------------------------------------------------------------------------*/

	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	// ARMAS
	
	/*
	 * Metodo para crear/dar de alta armas en el ArrayList de la clase Arma
	 * Recibe:
	 * @param nombre	nombre de arma
	 * @param peso		peso de arma
	 * @param dmg 		da�o de arma
	 */
	public void altaArma(String nombre, int peso, int dmg) {
		for (Arma arma : listaArmas) {
			if (!arma.getNombre().equals(nombre)) {
				listaArmas.add(new Arma(nombre, peso, dmg));
			} else {
				System.out.println("El arma que quieres crear ya existe :(");
			}
		}
	}
	
	/*
	 * Metodo para listar armas del ArrayList de la clase Arma
	 */
	public void listarArmas() {
		for (Arma arma : listaArmas) {
			if (arma != null) {
				System.out.println(arma);
			} else {
				System.out.println("Vaya, est� vacia la lista :(");
			}
		}
	}
	/*------------------------------------------------------------------------------------------------------------------------*/

	
	/*------------------------------------------------------------------------------------------------------------------------*/
	// FICHEROS SECUENCIALES
	
	/*
	 * Metodo para guardar datos de personajes en un fichero llamado "datosPersonaje.txt"
	 */
	public void guardarDatos() {
		try {
			File fichero = new File("src/ficheros/datosPersonaje.txt");
			if (!fichero.exists()) {
				fichero.createNewFile();
			}
			ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream(new File("src/ficheros/datosPersonaje.txt")));
			escribir.writeObject(listaPersonajes);
			escribir.close();
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo para introducir los datos para crear un personaje en el main
	 * y luego crearlo con el m�todo de altaPersonaje()
	 */
	public void datosPersonaje() {
		System.out.println("Nombre del personaje:");
		String nombre = escaner.nextLine();
		System.out.println("Codigo del personaje:");
		String codigo = escaner.nextLine();
		escaner.nextLine();
		System.out.println("Da�o del personaje:");
		int dmg = escaner.nextInt();
		System.out.println("Vida del personaje:");
		int hp = escaner.nextInt();
		escaner.nextLine();
		altaPersonaje(codigo, nombre, dmg, hp);
	}
	
	/*
	 * Metodo que sirve para cargar los datos de un fichero llamado "datosPersonaje.txt"
	 * al ArrayList de la clase Personaje
	 * tiene @SupressWarnings("unchecked")
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void cargarDatosPersonaje() {
		try {
			ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			System.out.println("Borramos datos del fichero datosPersonaje.txt");
			listaPersonajes.clear();
			System.out.println(listaPersonajes);
			listaPersonajes = (ArrayList<Personaje>) leer.readObject();
			System.out.println("Datos cargados");
			leer.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Fichero vacio");
		}
	}
	
	/*
	 * Metodo para visualizar los datos del fichero "datosPersonaje.txt"
	 */
	@SuppressWarnings("unchecked")
	public void verFichero() {
		ObjectInputStream escritura;
		try {
			escritura = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			System.out.println((ArrayList<Personaje>) escritura.readObject());
		}catch (IOException | ClassNotFoundException e) {
			System.out.println("Fichero vacio");
		}
	}
	
	/*
	 * Metodo que sirve para buscar datos del fichero "datosPersonaje.txt"
	 */
	@SuppressWarnings("unchecked")
	public void buscarPersonaje() {
		String nombre;
		System.out.println("Introduce el nombre del personaje que quieres buscar:");
		nombre = escaner.nextLine();
		ArrayList<Personaje> buscar = new ArrayList<Personaje>();
		
		try {
			ObjectInputStream escribir = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			
			buscar = (ArrayList<Personaje>) escribir.readObject();
			int contador = 0;
			
			for (Personaje personaje : buscar) {
				if (personaje.getNombre().equals(nombre)){
					System.out.println(personaje);
					contador++;
				}
			}
			
			if (contador == 0) {
				System.out.println("No se ha encontrado el personaje con ese nombre");
			}
			escribir.close();
		} catch (Exception e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo para encontrar al personaje con m�s da�o
	 * dentro de los datos almacenados en el fichero "datosPersonaje.txt"
	 */
	public void personajeConMasDmg() {
		try {
			ArrayList<Personaje> personajes = new ArrayList<Personaje>();
			ObjectInputStream escribir = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			
			int masDmg = 0;
			String nombreMasDmg = "";
			for (Personaje personaje : personajes) {
				if (personaje.getDmg() > masDmg) {
					nombreMasDmg = personaje.getNombre();
				} 
			}
			System.out.println(nombreMasDmg);
			File fichero = new File("src/ficheros/datosPersonajeMasDmg.txt");
			if (!fichero.exists()) {
				fichero.createNewFile();
			}
			ObjectOutputStream escritura = new ObjectOutputStream(new FileOutputStream(new File("src/ficheros/datosPersonajeMasDmg.txt")));
			ObjectInputStream lectura = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonajeMasDmg.txt")));
			System.out.println(lectura.readObject());
			escribir.close();
			escritura.close();
			lectura.close();
		} catch (Exception e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo para cambiar a may�sculas el nombre de un personaje
	 * dentro de los datos del fichero "datosPersonaje.txt"
	 */
	public void nombreMayusculas() {
		try {
			ObjectInputStream lectura = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			ObjectOutputStream escritura = new ObjectOutputStream(new FileOutputStream(new File("src/ficheros/datosPersonaje.txt")));
			for (Personaje  personaje : listaPersonajes) {
				String nombre = personaje.getNombre().toUpperCase();
				escritura.writeObject(nombre);
			}
			
			ObjectInputStream lecturaNombre = new ObjectInputStream(new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			for (Personaje personaje : listaPersonajes) {
				System.out.println(personaje);
			}
			
			escritura.close();
			lectura.close();
			lecturaNombre.close();
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo para cambiar el nombre de todos los personajes 
	 * dentro del fichero "datosPersonaje.txt"
	 */
	public void cambiarNombreMayus() {
		try {
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/datosPersonaje.txt")));

			for (Personaje personaje : listaPersonajes) {
				String nombre = personaje.getNombre().toUpperCase();
				escritura.writeObject(nombre);
			}

			ObjectInputStream lecturaNombre = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			for (int i = 0; i < listaPersonajes.size(); i++) {
				System.out.println(lecturaNombre.readObject());
			}

			escritura.close();
			lecturaNombre.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void contarCantidadPersonajes() {
		try {
			ObjectOutputStream escritura = new ObjectOutputStream(
					new FileOutputStream(new File("src/ficheros/datosPersonaje.txt")));
			
			ObjectInputStream lecturaNombre = new ObjectInputStream(
					new FileInputStream(new File("src/ficheros/datosPersonaje.txt")));
			int contador = 0;
			for (int i = 0; i < listaPersonajes.size(); i++) {
				contador++;
			}
			System.out.println("Existen " + contador + " personajes actualmente");
			
			escritura.close();
			lecturaNombre.close();
		} catch (IOException e) {
			System.out.println("Errorsito");
		}
	}
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	//	FICHEROS ALEATORIOS
	
	/*
	 * Metodo para a�adir armas a un cichero de acceso aleatorio
	 * habr� que introducir los datos de armas
	 * escribir� los datos en un un fichero llamado "listaArmasAleatorio"
	 */
	public void altaArmaFichero() {
		try {
			RandomAccessFile fichero = new RandomAccessFile("src/ficheros/listaArmasAleatorio", "rw");
			
			fichero.seek(fichero.length());
			String continuar = "";
			do {
				System.out.println("Nombre del arma:");
				String nombre = escaner.nextLine();
				fichero.writeUTF(nombre);
				System.out.println("Peso del aram:");
				int peso = escaner.nextInt();
				fichero.writeInt(peso);
				System.out.println("Da�o del arma");
				int dmg = escaner.nextInt();
				fichero.writeInt(dmg);
				escaner.nextLine();
				System.out.println("Si quieres crear otra arma escribe 'si'");
				continuar = escaner.nextLine();
			} while (continuar.equalsIgnoreCase("Si"));
			
			fichero.close();
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo que muestra el contendio del fichero "listaArmasAleatorio"
	 */
	
	public void verFicheroArmas() {
		try {
			RandomAccessFile fichero = new RandomAccessFile("src/ficheros/listaArmasAleatorio", "rw");
			
			String nombre= "";
			int peso = 0;
			int dmg = 0;
			boolean finFichero = false;
			do {
				try {
					nombre = fichero.readUTF();
					System.out.println("Arma nombre: " + nombre);
					peso = fichero.readInt();
					System.out.println("Arma peso: " + peso);
					dmg = fichero.readInt();
					System.out.println("Arma dmg " + dmg);
				} catch (EOFException e) {
					System.out.println("Fin del fichero");
					finFichero = true;
					fichero.close();
				}
			} while(!finFichero);
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	/*
	 * Metodo para mostrar los nombres de las armas
	 * almacenadas en el fichero "listaArmasAleatorio"
	 */
	public void mostrarNombreArmas() {
		System.out.println("Introduce el dmg del arma que quieres mostrar");
		String dmg = escaner.nextLine();
		if (dmg != null) {
		try {
			RandomAccessFile fichero = new RandomAccessFile("src/ficheros/listaArmasAleatorio", "rw");
			String nombre;
			boolean finFichero = false;
			System.out.println("Nombre de las armas");
			do {
				try {
					nombre = fichero.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					try {
						fichero.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					finFichero = true;
				} catch(IOException e) {
					e.printStackTrace();
				}
				
			} while(!finFichero);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	} else {
		System.out.println("El da�o es nulo");
	}
		
	}
	
	/*
	 * Metodo que muestra el tama�o en bytes del fichero "listaArmasAleatorio"
	 */

	public void longitudDelFichero() {
		try {
			RandomAccessFile longitud = new RandomAccessFile("src/ficheros/listaArmasAleatorio", "rw");
			System.out.println("El tama�o del fichero en bytes es de: " + longitud.length());
			longitud.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*------------------------------------------------------------------------------------------------------------------------*/
}
