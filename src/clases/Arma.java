package clases;

import java.io.Serializable;

public class Arma implements Serializable{
	
	/*
	 * @author Iv�n Calvo Burguete
	 */

	/**
	 * Atributos de la clase Arma
	 * @param nombre 	nombre del arma
	 * @param peso		peso del arma
	 * @param dmg		da�o del arma
	 */
	private static final long serialVersionUID = 1L;
	protected String nombre;
	protected int peso;
	protected int dmg;
	
	/*
	 * Constructor de la clase Arma
	 */
	public Arma(String nombre, int peso, int dmg) {
		super();
		this.nombre = nombre;
		this.peso = peso;
		this.dmg = dmg;
	}
	/*
	 * Metodo para mostrar el nombre
	 * returns nombre de arma
	 */
	public String getNombre() {
		return nombre;
	}

	/*
	 * Metodo para dar nombre a un arma
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/*
	 * Metodo para mostrar el peso de un arma
	 * returns peso de arma
	 */
	public int getPeso() {
		return peso;
	}
	/*
	 * Metodo para dar peso a un arma
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}
	/*
	 * Metodo para mostrar el da�o de un arma
	 * returns dmg de un arma
	 */
	public int getDmg() {
		return dmg;
	}
	
	/*
	 * Metodo para dar valor al da�o de un arma
	 */
	public void setDmg(int dmg) {
		this.dmg = dmg;
	}
	/*
	 * Javadoc
	 * Metodo que muestra los datos de la clase Arma
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Arma [nombre=" + nombre + ", peso=" + peso + ", dmg=" + dmg + "]";
	}

}
