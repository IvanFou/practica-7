package clases;

import java.io.Serializable;

public class Personaje implements Serializable{
	
	/*
	 * @author Iv�n Calvo Burguete
	 */

	/**
	 * Atributos del personaje, todos con private y para poder
	 * trabajar con ellos se usar�n setters y getters
	 */
	private static final long serialVersionUID = 1L;
	private String codigo;
	private String nombre;
	private int dmg;
	private int hp;
	
	/*
	 * COnsturctor de personaje, hay que introducir sus atributos para poder crearlo
	 * @param codigo	codigo persona
	 * @param nombre	nombre personaje
	 * @param dmg		da�o personaje
	 * @param hp		vida personaje
	 */
	public Personaje(String codigo, String nombre, int dmg, int hp) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.dmg = dmg;
		this.hp = hp;
	}
	/*
	 * Metodo para mostrar el codigo de un personaje
	 * returns codigo de personaje
	 */
	public String getCodigo() {
		return codigo;
	}
	/*
	 * Metodo para dar valor al c�digo de un personaje
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/*
	 * Metodo para mostrar el nombre de un personaje
	 * return nombre de personaje
	 */
	public String getNombre() {
		return nombre;
	}
	/*
	 * Metodo para dar valor al nombre de un personaje
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/*
	 * Metodo para mostrar el da�o de un personaje
	 * returns dmg de personaje
	 */
	public int getDmg() {
		return dmg;
	}
	/*
	 * Metodo para asignar un valor al da�o de un personaje
	 */
	public void setDmg(int dmg) {
		this.dmg = dmg;
	}
	/*
	 * Metodo para ver la vida de un personaje
	 * return hp de personaje
	 */
	public int getHp() {
		return hp;
	}
	/*
	 * Metodo para dar valor a la vida de un personaje
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}
	/*
	 * Javadoc
	 * Metodo para mostrar todos los datos de la clase Personaje
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Personaje [codigo=" + codigo + ", nombre=" + nombre + ", dmg=" + dmg + ", hp=" + hp + "]";
	}
	
	
}
